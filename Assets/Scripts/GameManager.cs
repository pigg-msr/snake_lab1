﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] private Snake snake;

    private LevelGrid levelGrid;

    // Start is called before the first frame update
    void Start()
    {
        levelGrid = new LevelGrid(16, 16);

        snake.Setup(levelGrid);
        levelGrid.Setup(snake);
    }

}
