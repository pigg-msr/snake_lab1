﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGrid
{
    private Vector2 foodGridPosition;
    private GameObject foodGameObject;
    private int width;
    private int height;
    private Snake snake;

    public LevelGrid(int width, int height)
    {
        this.width = width;
        this.height = height;
    }

    public void Setup(Snake snake)
    {
        this.snake = snake;
        SpawnFood();
    }

    private void SpawnFood()
    {
        foodGridPosition = new Vector2(Random.Range(0, width), Random.Range(0, height));
        while (!snake.EmptySpotOnGrid(foodGridPosition))
        {
            foodGridPosition = new Vector2(Random.Range(0, width), Random.Range(0, height));
        }
        foodGameObject = new GameObject("Food", typeof(SpriteRenderer));
        foodGameObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.foodSprite;
        foodGameObject.transform.position = new Vector3(foodGridPosition.x, foodGridPosition.y);
    }

    public bool TrySnakeEatFood(Vector2 snakePosition)
    {
        if(snakePosition == foodGridPosition)
        {
            Object.Destroy(foodGameObject);
            SpawnFood();
            return true;
        }
        return false;
    }

    public Vector2 Warp (Vector2 snakeHeadPosition)
    {
        if(snakeHeadPosition.x < 0)
        {
            snakeHeadPosition.x = width - 1;
        }
        if (snakeHeadPosition.x > width - 1)
        {
            snakeHeadPosition.x = 0;
        }
        if (snakeHeadPosition.y < 0)
        {
            snakeHeadPosition.y = height - 1;
        }
        if (snakeHeadPosition.y > height - 1)
        {
            snakeHeadPosition.y = 0;
        }
        return snakeHeadPosition;
    }
}
