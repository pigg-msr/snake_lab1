﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Snake : MonoBehaviour
{
    private enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }

    private enum State
    {
        Alive,
        Dead
    }
    private State state;
    private Direction sneakMoveDirection;
    private Vector2 snakeHeadPosition;
    private float moveTimer;
    private float moveTimerMax;
    private LevelGrid levelGrid;
    private int snakeTailSize;
    private List<TailElement> snakeTail;
    private AudioSource audio;
    private bool snakeAteFood;
    
    public AudioClip foodCollectSound;

    public Text scoreText;
    public Text deadText;
    private int score = 0;

    private Direction actualDirection;
    private Vector2 previousSnakePosition;

    public void Setup(LevelGrid levelGrid)
    {
        this.levelGrid = levelGrid;
        audio = gameObject.AddComponent<AudioSource>();
    }

    private void Awake()
    {
        snakeHeadPosition = new Vector2(8f, 8f);
        moveTimerMax = 1f;
        moveTimer = moveTimerMax;
        sneakMoveDirection = Direction.Right;
        actualDirection = sneakMoveDirection;
        snakeTailSize = 0;
        snakeTail = new List<TailElement>();

    }

    private void Update()
    {
        HandleInput();
        
        
    }

    private void FixedUpdate()
    {
        switch (state)
        {
            case State.Alive:
                HandleGridMovement();
                break;
            case State.Dead:
                break;
        }
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if(actualDirection != Direction.Down){
                sneakMoveDirection = Direction.Up;
            }
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (actualDirection != Direction.Up)
            {
                sneakMoveDirection = Direction.Down;
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (actualDirection != Direction.Right)
            {
                sneakMoveDirection = Direction.Left;
            }
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (actualDirection != Direction.Left)
            {
                sneakMoveDirection = Direction.Right;
            }
        }
    }

    private void HandleGridMovement()
    {
        moveTimer += Time.deltaTime * 6;
        if(moveTimer >= moveTimerMax)
        {
            previousSnakePosition = snakeHeadPosition;
            actualDirection = sneakMoveDirection;
            moveTimer -= moveTimerMax;

            Vector2 gridMoveDirectionVector;
            switch (sneakMoveDirection)
            {
                default:
                case Direction.Right: gridMoveDirectionVector = new Vector2(+1, 0); break;
                case Direction.Left: gridMoveDirectionVector = new Vector2(-1, 0); break;
                case Direction.Up: gridMoveDirectionVector = new Vector2(0, +1); break;
                case Direction.Down: gridMoveDirectionVector = new Vector2(0, -1); break;
            }
            snakeHeadPosition += gridMoveDirectionVector;
            snakeHeadPosition = levelGrid.Warp(snakeHeadPosition);

            snakeAteFood = levelGrid.TrySnakeEatFood(snakeHeadPosition);
            if (snakeAteFood)
            {
                CreateTailElement();
                scoreText.text = "Score: " + ++score;
            }
            

            foreach(TailElement tail in snakeTail)
            {
                Vector2 tailGridPosition = tail.GetTailPosition();
                
                    if (snakeHeadPosition == tailGridPosition)
                    {
                        if (snakeTailSize >= 2)
                        {
                            deadText.text = "umiernako";
                            state = State.Dead;
                        }
                    }
            }
            
            transform.position = new Vector3(snakeHeadPosition.x, snakeHeadPosition.y);
            transform.eulerAngles = new Vector3(0, 0, GetAngleFromVector(gridMoveDirectionVector) - 90);
            UpdateTail();
        }
    }
    
    private static float GetAngleFromVector(Vector2 dir)
    {
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (n < 0) n += 360;
        return n;
    }

    private void CreateTailElement()
    {
        snakeTail.Add(snakeTailSize == 0
            ? new TailElement(snakeHeadPosition)
            : new TailElement(snakeTail[snakeTail.Count - 1].GetTailPosition()));
        snakeTailSize++;
        audio.PlayOneShot(foodCollectSound);
    }

    private void UpdateTail()
    {
        var len = snakeTail.Count;
        if (len == 0) return;

        for (var i = len - 1; i > 0; i--)
        {
            snakeTail[i].SetTailPosition(snakeTail[i - 1].GetTailPosition());
        }
        snakeTail[0].SetTailPosition(previousSnakePosition);
    }

    public bool EmptySpotOnGrid(Vector2 vector)
    {
        foreach (var tailElement in snakeTail)
        {
            
            if (tailElement.GetTailPosition().Equals(vector)) return false;
        }

        if (snakeHeadPosition.Equals(vector)) return false;
        
        return true;
    }

    private class TailElement
    {
        private GameObject tailObject;
        private Vector2 tailPosition;
        private bool waitForFirstMove;

        public TailElement(Vector2 lastTailElementPosition)
        {
            waitForFirstMove = true;
            tailPosition = lastTailElementPosition;
            tailObject= new GameObject("SnakeBody", typeof(SpriteRenderer));
            tailObject.GetComponent<SpriteRenderer>().sprite = GameAssets.instance.snakeBodySprite;
            tailObject.transform.position = new Vector3(tailPosition.x, tailPosition.y);
        }

        public Vector2 GetTailPosition()
        {
            return tailPosition;
        }

        public void SetTailPosition(Vector2 newPosition)
        {
            if (waitForFirstMove)
            {
                waitForFirstMove = false;
                return;
            }
            tailPosition = newPosition;
            tailObject.transform.position = new Vector3(tailPosition.x, tailPosition.y);
        }
    }
}
